﻿using System.Linq;

namespace PersistentBugger
{
    public class Kata
    {
        public static int Persistence(long n)
        {
            return n < 10 
                ? 0 
                : Persistence(
                    n.ToString()
                    .Select(x => long.Parse(x.ToString()))
                    .Aggregate((pre, now) => pre * now)) 
                    + 1;
        }
    }
}
